/**
 * The VEGAS JS 'signals' library is a light-weight messaging tools. - version: 1.3.7 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = global || self, global.signals = factory());
}(this, function () { 'use strict';

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);

      if (enumerableOnly) {
        symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
      }

      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();

    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
          result;

      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn(this, result);
    };
  }

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var Receiver = function () {
    function Receiver() {
      _classCallCheck(this, Receiver);
    }
    _createClass(Receiver, [{
      key: "receive",
      value: function receive() {}
    }, {
      key: "toString",
      value: function toString() {
        return '[' + this.constructor.name + ']';
      }
    }]);
    return Receiver;
  }();

  var Signaler = function () {
    function Signaler() {
      _classCallCheck(this, Signaler);
    }
    _createClass(Signaler, [{
      key: "length",
      get: function get() {
        return 0;
      }
    }, {
      key: "connect",
      value: function connect(receiver) {
      }
    }, {
      key: "connected",
      value: function connected() {}
    }, {
      key: "disconnect",
      value: function disconnect() {
      }
    }, {
      key: "emit",
      value:
      function emit() {}
    }, {
      key: "hasReceiver",
      value: function hasReceiver(receiver) {
        return false;
      }
    }, {
      key: "toString",
      value: function toString() {
        return '[Signaler]';
      }
    }]);
    return Signaler;
  }();

  var Signal = function (_Signaler) {
    _inherits(Signal, _Signaler);
    var _super = _createSuper(Signal);
    function Signal() {
      var _this;
      _classCallCheck(this, Signal);
      _this = _super.call(this);
      Object.defineProperties(_assertThisInitialized(_this), {
        proxy: {
          value: null,
          configurable: true,
          writable: true
        },
        receivers: {
          writable: true,
          value: []
        }
      });
      return _this;
    }
    _createClass(Signal, [{
      key: "length",
      get: function get() {
        return this.receivers.length;
      }
    }, {
      key: "connect",
      value: function connect(receiver) {
        var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var autoDisconnect = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        if (receiver === null) {
          return false;
        }
        autoDisconnect = autoDisconnect === true;
        priority = priority > 0 ? priority - priority % 1 : 0;
        if (typeof receiver === "function" || receiver instanceof Function || receiver instanceof Receiver || "receive" in receiver) {
          if (this.hasReceiver(receiver)) {
            return false;
          }
          this.receivers.push(new SignalEntry(receiver, priority, autoDisconnect));
          var i;
          var j;
          var a = this.receivers;
          var swap = function swap(j, k) {
            var temp = a[j];
            a[j] = a[k];
            a[k] = temp;
            return true;
          };
          var swapped = false;
          var l = a.length;
          for (i = 1; i < l; i++) {
            for (j = 0; j < l - i; j++) {
              if (a[j + 1].priority > a[j].priority) {
                swapped = swap(j, j + 1);
              }
            }
            if (!swapped) {
              break;
            }
          }
          return true;
        }
        return false;
      }
    }, {
      key: "connected",
      value: function connected() {
        return this.receivers.length > 0;
      }
    }, {
      key: "disconnect",
      value: function disconnect() {
        var receiver = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        if (receiver === null) {
          if (this.receivers.length > 0) {
            this.receivers = [];
            return true;
          } else {
            return false;
          }
        }
        if (this.receivers.length > 0) {
          var l = this.receivers.length;
          while (--l > -1) {
            if (this.receivers[l].receiver === receiver) {
              this.receivers.splice(l, 1);
              return true;
            }
          }
        }
        return false;
      }
    }, {
      key: "emit",
      value: function emit() {
        var l = this.receivers.length;
        if (l === 0) {
          return;
        }
        var i;
        var r = [];
        var a = this.receivers.slice();
        var e;
        var slot;
        for (i = 0; i < l; i++) {
          e = a[i];
          if (e.auto) {
            r.push(e);
          }
        }
        if (r.length > 0) {
          l = r.length;
          while (--l > -1) {
            i = this.receivers.indexOf(r[l]);
            if (i > -1) {
              this.receivers.splice(i, 1);
            }
          }
        }
        l = a.length;
        for (var _len = arguments.length, values = new Array(_len), _key = 0; _key < _len; _key++) {
          values[_key] = arguments[_key];
        }
        for (i = 0; i < l; i++) {
          slot = a[i].receiver;
          if (slot instanceof Function || typeof slot === "function") {
            slot.apply(this.proxy || this, values);
          } else if (slot instanceof Receiver || "receive" in slot && slot.receive instanceof Function) {
            slot.receive.apply(this.proxy || slot, values);
          }
        }
      }
    }, {
      key: "hasReceiver",
      value: function hasReceiver(receiver) {
        if (receiver === null) {
          return false;
        }
        if (this.receivers.length > 0) {
          var l = this.receivers.length;
          while (--l > -1) {
            if (this.receivers[l].receiver === receiver) {
              return true;
            }
          }
        }
        return false;
      }
    }, {
      key: "toArray",
      value: function toArray() {
        if (this.receivers.length > 0) {
          return this.receivers.map(function (item) {
            return item.receiver;
          });
        }
        return [];
      }
    }, {
      key: "toString",
      value: function toString() {
        return '[Signal]';
      }
    }]);
    return Signal;
  }(Signaler);
  var SignalEntry = function () {
    function SignalEntry(receiver) {
      var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var auto = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      _classCallCheck(this, SignalEntry);
      this.auto = auto;
      this.receiver = receiver;
      this.priority = priority;
    }
    _createClass(SignalEntry, [{
      key: "toString",
      value: function toString() {
        return '[SignalEntry]';
      }
    }]);
    return SignalEntry;
  }();

  /**
   * The {@link signals} library is a light-weight messaging tools.
   * <p><b>Concept: </b>
   * <ul>
   * <li>A Signal is essentially a minimal emiter specific to one event, with its own <code>array</code> of receivers/slots ({@link system.signals.Receiver|Receiver} or <code>Function</code>).</li>
   * <li>A Signal gives an event a concrete membership in a class.</li>
   * <li>Receivers subscribe to real objects, not to string-based channels.</li>
   * <li>Event string constants are no longer needed.</li>
   * <li>Signals are inspired by {@link https://en.wikipedia.org/wiki/Signals_and_slots|signals/slots in Qt}.</li>
   * <ul>
   * @summary The {@link system.signals} library is light-weight, strongly-typed messaging tools.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace signals
   * @version 1.2.0
   * @since 1.0.0
   * @example
   * function Slot( name )
   * {
   *     this.name = name ;
   * }
   *
   * Slot.prototype = Object.create( system.signals.Receiver.prototype );
   * Slot.prototype.constructor = Slot;
   *
   * Slot.prototype.receive = function ( message )
   * {
   *     trace( this + " : " + message ) ;
   * }
   *
   * Slot.prototype.toString = function ()
   * {
   *     return "[Slot name:" + this.name + "]" ;
   * }
   *
   * var slot1 = new Slot("slot1") ;
   *
   * var slot2 = function( message )
   * {
   *     trace( this + " : " + message ) ;
   * }
   *
   * var signal = new system.signals.Signal() ;
   *
   * //signal.proxy = slot1 ;
   *
   * signal.connect( slot1 , 0 ) ;
   * signal.connect( slot2 , 2 ) ;
   *
   * signal.emit( "hello world" ) ;
   */
  var props = {
    Receiver: Receiver,
    Signaler: Signaler,
    Signal: Signal
  };

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-signals'
    },
    description: {
      enumerable: true,
      value: "The VEGAS JS 'signals' library is a light-weight messaging tools."
    },
    version: {
      enumerable: true,
      value: '1.3.7'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-signals'
    }
  });
  var bundle = _objectSpread2({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, props);
  try {
    var load = function load() {
      window.removeEventListener("load", load, false);
      sayHello(metas.name, metas.version, metas.url);
    };
    if (window) {
      window.addEventListener('load', load, false);
    }
  } catch (ignored) {}

  return bundle;

}));
//# sourceMappingURL=vegas.signals.js.map
