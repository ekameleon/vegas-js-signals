"use strict" ;

import sayHello , { skipHello } from 'vegas-js-core/src/hello'

import props from './index'

const metas = Object.defineProperties( {} ,
{
    name        : { enumerable : true , value : '<@NAME@>'        },
    description : { enumerable : true , value : "<@DESCRIPTION@>" },
    version     : { enumerable : true , value : '<@VERSION@>'     } ,
    license     : { enumerable : true , value : "<@LICENSE@>"     } ,
    url         : { enumerable : true , value : '<@HOMEPAGE@>'    }
});

const bundle =
{
      metas ,
      sayHello,
      skipHello,
      ...props
};

export default bundle ;

try
{
    const load = () =>
    {
        window.removeEventListener( "load" , load , false ) ;
        sayHello(metas.name,metas.version,metas.url) ;
    };
    if ( window )
    {
        window.addEventListener( 'load' , load , false );
    }
}
catch( ignored ) {}