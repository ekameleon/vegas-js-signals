/* globals signals */
"use strict" ;

window.onload = function()
{
    if( !signals )
    {
        throw new Error("The VEGAS Signals library is undefined.") ;
    }
    
    // ------
    
    const { Receiver , Signal , metas } = signals ;
    
    console.log( metas ) ;

    function Slot( name )
    {
        this.name = name ;
    }

    Slot.prototype = Object.create( Receiver.prototype );
    Slot.prototype.constructor = Slot;

    Slot.prototype.receive = function ( message )
    {
        console.log( this + " : " + message ) ;
    };

    Slot.prototype.toString = function ()
    {
        return "[Slot name:" + this.name + "]" ;
    };

    // ------

    let slot1 = new Slot("slot1") ;

    let slot2 = function( message )
    {
        console.log( this + " : " + message ) ;
    };

    let signal = new Signal() ;

    //signal.proxy = slot1 ;

    signal.connect( slot1 , 0 ) ;
    signal.connect( slot2 , 2 ) ;

    console.log( "signal.connected : " + signal.connected() ) ;
    console.log( "signal.length : "    + signal.length ) ;
    console.log( "signal.hasReceiver(slot1) : " + signal.hasReceiver(slot1) ) ;
    console.log( "signal.hasReceiver(slot2) : " + signal.hasReceiver(slot2) ) ;

    signal.emit( "hello world" ) ;

    signal.disconnect( slot1 ) ;

    signal.emit( "Bonjour monde" ) ;
};