const presets =
[
    [ "@babel/preset-env" , { "modules" : "auto" } ]
];

const plugins =
[
    [ "@babel/plugin-transform-runtime" , { helpers:false , useESModules:true } ]
];

module.exports = { presets, plugins };