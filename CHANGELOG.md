# VEGAS JS CORE OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.3.7] - 2021-05-14
### Changed
* Change vegas-js-core dependency version : 1.0.32
* Update all the package.json dependencies

## [1.3.6] - 2020-03-17
### Changed
* Change vegas-js-core dependency version : 1.0.19

## [1.3.5] - 2019-08-21
### Changed
* Change vegas-js-core dependency version : 1.0.17

## [1.3.4] - 2019-04-16
### Changed
* Change vegas-js-core dependency version : 1.0.15

## [1.3.3] - 2018-11-08
### Changed
* Add a constructor in the Receiver class

## [1.3.2] - 2018-11-03
### Changed
* Change vegas-js-core dependency version : 1.0.5

## [1.3.1] - 2018-10-28
### Changed
* Upgrade the package.json file, complete the files definition

## [1.3.0] - 2018-10-28
### Changed
* Refactoring and fix builds

## [1.2.2] - 2018-10-25
### Added
* More ES6 + refactoring

## [1.2.1] - 2018-10-25
### Added
* Change the main package architecture and use default export only

## [1.0.0] - 2018-03-07
### Added
* First stable version

