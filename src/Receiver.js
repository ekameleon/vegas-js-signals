"use strict" ;

/**
 * The {system.signals.Receiver|Receiver} interface is the primary method for receiving values from Signal objects.
 * @name Receiver
 * @interface
 * @memberof system.signals
 */
export default class Receiver
{
    constructor()
    {
    }
    
    /**
     * This method is called when the receiver is connected with a Signal object.
     * @memberof system.signals.Receiver
     * @function
     * @instance
     */
    receive() {}
    
    /**
     * Returns the string representation of the object.
     * @memberof system.signals.Receiver
     * @return {string} The string representation of the object.
     */
    toString()
    {
        return '[' + this.constructor.name + ']' ;
    }
}

