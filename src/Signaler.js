/*jslint unused: false */
"use strict" ;

/**
 * The Signaler interface is the primary method for emit messages.
 * @name Signaler
 * @interface
 * @memberof system.signals
 */
export default class Signaler
{
    constructor()
    {
    
    }
    
    /**
     * Indicates the number of receivers connected.
     * @memberof system.signals.Signaler
     * @readonly
     * @instance
     */
    get length()
    {
        return 0 ;
    }
    
    /**
     * Connects a Function or a Receiver object.
     * @param {system.signals.Receiver|Function} receiver The receiver to connect : a Function reference or a Receiver object.
     * @param {number} [priority=0] Defines the priority level of the receiver.
     * @param {boolean} [autoDisconnect=false] Apply a disconnect after the first trigger
     * @return {boolean} A <code>true</code> value If the receiver is connected with the signal emitter.
     * @memberof system.signals.Signaler
     * @instance
     * @function
     */
    connect( receiver , priority = 0 , autoDisconnect = false )
    {
    
    }
    
    /**
     * Returns <code>true</code> if one or more receivers are connected.
     * @memberof system.signals.Signaler
     * @instance
     * @function
     * @return {boolean} <code>true</code> if one or more receivers are connected.
     */
    connected()
    {
    
    }
    
    /**
     * Disconnect the specified object or all objects if the parameter is null.
     * @param {system.signals.Receiver|Function} [receiver=null] The receiver to disconnect : a Function reference or a Receiver object.
     * @memberof system.signals.Signaler
     * @instance
     * @function
     * @return {boolean} A <code>true</code> if the specified receiver exist and can be disconnected.
     */
    disconnect( receiver = null ) {} ;
    
    /**
     * Emit the specified values to the receivers.
     * @param {...*} values - All values to emit to the receivers.
     * @memberof system.signals.Signaler
     * @instance
     * @function
     */
    emit( ...values )
    {
    
    }
    
    /**
     * Returns <code>true</code> if the specified receiver is connected.
     * @param {system.signals.Receiver|Function} receiver - The specific slot to find in the register list of the signal.
     * @return {boolean} A <code>true</code> value if the specified receiver is connected.
     * @memberof system.signals.Signaler
     * @instance
     * @function
     */
    hasReceiver( receiver )
    {
        return false ;
    }
    
    /**
     * Returns the String representation of the object.
     * @returns {string} The string representation of the object.
     */
    toString()
    {
        return '[Signaler]' ;
    }
}
